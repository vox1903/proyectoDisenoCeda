﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using proyectoDiseno2.Clases.FileHandling;

namespace proyectoDiseno2.Output
{
    interface FileOutput
    {
        void printTemplate(Template template);
    }
}

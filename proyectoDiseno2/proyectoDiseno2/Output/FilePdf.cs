﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proyectoDiseno2.Clases.FileHandling;
using System.IO;
using System.Web.UI.WebControls;

namespace proyectoDiseno2.Output
{
    public class FilePdf : FileOutput
    {
       
        public FilePdf()
        {
            
        }

        public void createDoc(String defaultFileName, Template temp)
        {
            LinkedList<Component> content = temp.getComponents();
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc,
              new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\"
               + defaultFileName + ".pdf", FileMode.Create));
            doc.AddTitle("Template");
            doc.AddCreator("CEDA");
            doc.Open();
            Paragraph paragraph = new Paragraph();
            paragraph.Add("Nombre Plantilla:" + temp.getName());
            paragraph.Add("\n\n");
            doc.Add(paragraph);
            createDocAux(doc,content);
            doc.Close();

        }

        public void createDocAux(Document doc, LinkedList<Component> content) {

            
            foreach (Component c in content) {

                Paragraph paragraph = new Paragraph();

                if (c is Part) {

                    paragraph.Add(c.getName());
                    paragraph.Add(":" + "\t");
                    paragraph.Add(c.getContent());
                    paragraph.Add("\n");
                    paragraph.Add("\n");
                    doc.Add(paragraph);
                }
                else if (c is Clases.FileHandling.Section) {
                    paragraph.Add("\n");
                    paragraph.Add(c.getName());
                    paragraph.Add("\n");
                    doc.Add(paragraph);
                    createDocAux(doc, ((Clases.FileHandling.Section)c).getComponents());

                }
            }

            
            

        }

        public void printTemplate(Template template)
        {
            throw new NotImplementedException();
        }

        

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using proyectoDiseno2.Clases;
using proyectoDiseno2.Clases.FileHandling;


namespace proyectoDiseno2.GUI
{
    public partial class Login : System.Web.UI.Page
    {
        ControllerWeb controllerWeb = new ControllerWeb();

        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = new ControllerWeb();
            //controllerWeb = (ControllerWeb)Session["item"];
        }

        protected void button_SignIn_Click(object sender, EventArgs e)
        {
            if (controllerWeb.signIn(textbox_ussername.Text, textBox_password.Text))
            {
                Session["Item"] = controllerWeb;

                if (controllerWeb.getCurrentUser().GetType() == typeof(Clases.Users.CurricularManager))
                {

                    Response.Redirect("Gestor.aspx");

                }
                else if (controllerWeb.getCurrentUser().GetType() == typeof(Clases.Users.Admin))
                {

                    Response.Redirect("Admin.aspx");

                }
                else
                {

                    Response.Redirect("Professor.aspx");

                }               
            }
        }



    }
}
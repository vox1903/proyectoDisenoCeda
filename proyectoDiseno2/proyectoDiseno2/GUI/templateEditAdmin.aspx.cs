﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using proyectoDiseno2.Clases;
using proyectoDiseno2.Clases.FileHandling;
using proyectoDiseno2.Clases.Users;
using System.Xml.Linq;
using System.IO;
using proyectoDiseno2.Output;

namespace proyectoDiseno2.GUI
{
    public partial class templateEditAdmin : System.Web.UI.Page
    {

        ControllerWeb controllerWeb;
        Template template;
        LinkedList<Tuple<Control,Control, Component>> controls;

        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = (ControllerWeb)Session["item"];
            controls = new LinkedList<Tuple<Control, Control, Component>>();
            Clases.Users.Admin user = ((Clases.Users.Admin)controllerWeb.getCurrentUser());
            template =  user.editTemplate(Request.QueryString["name"]);
            template.setState(Template.State.DRAFT);
            printTemplate();
        }


        public void printTemplate()
        {
            printTemplateAux(template.getComponents());
        }

        private void printTemplateAux(LinkedList<Component> comp)
        {

            foreach (Component c in comp)
            {
                TextBox label;
                TextBox text;
                if (c is Part)
                {
                    label = new TextBox();
                    label.Text = c.getName();
                    label.Width = 250;
                    text = new TextBox();
                    text.Text = c.getContent();
                    text.Width = 500;
                    panelTemplate.Controls.Add(label);
                    panelTemplate.Controls.Add(text);
                    controls.AddLast(new Tuple<Control,Control, Component>(label,text, c));
                }
                else if (c is Section)
                {
                    label = new TextBox();
                    label.Text = c.getName();
                    label.Width = 750;
                    panelTemplate.Controls.Add(label);
                    controls.AddLast(new Tuple<Control, Control, Component>(label, null, c));
                    printTemplateAux(((Section)c).getComponents());
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            if (newName.Text.ToString().Trim().Equals(""))
            {
                return;
            }
            template.setName(newName.Text);
            
            foreach (Tuple<Control,Control, Component> t in controls)
            {
                if (t.Item3 is Section)
                {
                    t.Item3.setName(((TextBox)t.Item1).Text);
                }
                else
                {
                    t.Item3.setName(((TextBox)t.Item1).Text);
                    t.Item3.setContent(((TextBox)t.Item2).Text);
                }
            }
            Clases.Users.Admin user = ((Clases.Users.Admin)controllerWeb.getCurrentUser());
            template.setState(Template.State.SAVED);
            template.setLocation(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial");
            user.saveChanges(template);

            XDocument doc = XDocument.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\names.xml");
            XElement names = doc.Element("templates");
            names.Add(new XElement("template",new XText(template.getName())));
            doc.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\names.xml");

            Response.Redirect("Admin.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (newName.Text.ToString().Trim().Equals(""))
            {
                return;
            }
            if (Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\Current"))
            {
                Directory.Delete(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\Current", false);
            }
            Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\Current");
            template.setName(newName.Text);

            Clases.Users.Admin user = ((Clases.Users.Admin)controllerWeb.getCurrentUser());
            template.setLocation(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\Current");
            template.setState(Template.State.DONE);
            user.saveChanges(template);

            Response.Redirect("Admin.aspx");

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            FilePdf pdf = new FilePdf();
            pdf.createDoc(template.getName(), template);
            
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="proyectoDiseno2.GUI.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://raw.githubusercontent.com/moment/moment/develop/src/moment.js"></script>
    <script type="text/javascript" src="https://raw.githubusercontent.com/twbs/bootstrap/master/js/collapse.js"></script>
    <script type="text/javascript" src="https://raw.githubusercontent.com/twbs/bootstrap/master/js/transition.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/estiloTemplate.css" rel="stylesheet" type="text/css"/>
</head>
<body>

     <div class="navbar navbar-default navbar-static-top">
     
        
         <b><font color="white">CEDA</font></b>
        
        
      </div>
   


    <div style=" padding-left: 0px; padding-top: 100px; padding-bottom: 5px; text-align:center">
    <form id="form1" runat="server">
        <b>Username</b>
        <div style=" padding-left: 0px; padding-top: 10px; padding-bottom: 5px; text-align:center">
            <asp:TextBox ID="textbox_ussername" runat="server"></asp:TextBox>
        </div>
        <b>Password</b>
        <div style=" padding-left: 0px; padding-top: 10px; padding-bottom: 15px; text-align:center">
        <asp:TextBox ID="textBox_password" runat="server" type="password"></asp:TextBox>
        </div>
   
            <b><asp:Button ID="button_SignIn" runat="server" OnClick="button_SignIn_Click" Text="Sign In" /></b>
                    
    </form>
    </div>


    <footer class="section section-primary">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h1>Footer header</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud</p>
          </div>
          <div class="col-sm-6">
            <p class="text-info text-right">
              <br>
              <br>
            </p>
            <div class="row">
              <div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 hidden-xs text-right">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

</body>
</html>

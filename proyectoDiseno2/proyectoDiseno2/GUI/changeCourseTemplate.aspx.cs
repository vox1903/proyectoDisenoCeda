﻿using proyectoDiseno2.Clases;
using proyectoDiseno2.Clases.FileHandling;
using proyectoDiseno2.Clases.Users;
using proyectoDiseno2.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace proyectoDiseno2.GUI
{
    public partial class changeCourseTemplate : System.Web.UI.Page
    {
        ControllerWeb controllerWeb;
        Template template;
        LinkedList<Tuple<Control,Component>> controls;
        FilePdf pdf;
       
        protected void Page_Load(object sender, EventArgs e)
        {

            controllerWeb = (ControllerWeb)Session["item"];
            template = controllerWeb.editTemplate("oficial");
            controls = new LinkedList<Tuple<Control, Component>>();


            printTemplate();

        }

        public void printTemplate()
        {
            printTemplateAux(template.getComponents());
        }

        private void printTemplateAux(LinkedList<Component> comp)
        {
                        
            foreach (Component c in comp) {
                Label label;
                TextBox text;
                if (c is Part)
                {
                    label = new Label();
                    label.Text = c.getName();
                    label.Width = 250;
                    text = new TextBox();
                    text.Text = c.getContent();
                    text.Width = 500;
                    panelTemplate.Controls.Add(label);
                    panelTemplate.Controls.Add(text);
                    controls.AddLast(new Tuple<Control, Component>(text, c));
                }
                else if (c is Section) {
                    label = new Label();
                    label.Text = c.getName();
                    label.Width = 750;
                    panelTemplate.Controls.Add(label);
                    printTemplateAux(((Section)c).getComponents());
                }
            }
        }


        protected void button_Save_click(object sender, EventArgs e)
        {
            foreach (Tuple<Control,Component> t in controls)
            {
                t.Item2.setContent(((TextBox)t.Item1).Text);
            }
            template.setState(Template.State.DRAFT);
        }


        protected void button_add_professor(object sender, EventArgs e)
        {
            String courseId = Request.QueryString["CourseId"];
            String planId = Request.QueryString["PlanId"];
            foreach (User user in controllerWeb.getUsers())
            {
                if (user.getUssername().Equals(textBox_password.Text))
                {
                    Response.Write("<p>User<p>");

                    foreach (CoursePlan plan in controllerWeb.getPlans()) {
                        //Response.Write("1." +plan.getId() + "2. " + planId);

                        if (plan.getId().Equals("IC 410")) {

                            Response.Write("<p>Plan<p>");

                            foreach (Course course in plan.getCourses()) {
                               // Response.Write("1." + course.getId() + " 2." + courseId);
                                if (course.getId().Equals("IC 6811"))
                                {
                                    //Response.Write("<p>Course<p>");
                                    ((Clases.Users.Professor)user).addCourse(course);
                                    
                                }

                            }

                        }
                        
                    }

                    
                }
            }
        }

    }
}
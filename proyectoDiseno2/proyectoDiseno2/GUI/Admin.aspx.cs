﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using proyectoDiseno2.Clases;

namespace proyectoDiseno2.GUI
{
    public partial class Admin : System.Web.UI.Page
    {
        ControllerWeb controllerWeb;

        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = (ControllerWeb)Session["item"];
        }


        public void printAvailableTemplates()
        {
            foreach (string name in controllerWeb.getNameTemplates())
            {
                Response.Write("<br><a href = templateEditAdmin.aspx?name=" +name + " >" + name + "</a>");
            }
        }

    }
}
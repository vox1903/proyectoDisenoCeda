﻿using proyectoDiseno2.Clases;
using proyectoDiseno2.Clases.FileHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace proyectoDiseno2.GUI
{
    public partial class Professor : System.Web.UI.Page
    {
        ControllerWeb controllerWeb;
   
        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = (ControllerWeb)Session["item"];
        }

        public void printCourses() {

            foreach (Course course in ((Clases.Users.Professor)controllerWeb.getCurrentUser()).getCourses())
            {
                Response.Write("<br><a href = \"./changeCourseTemplate.aspx?CourseId=" + course.getId() + "\">" + course.getId() + " - " + course.getName() + "</a>");
            }
        }
    }
}
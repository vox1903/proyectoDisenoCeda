﻿using proyectoDiseno2.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using proyectoDiseno2.Clases.FileHandling;

namespace proyectoDiseno2.GUI
{
    public partial class Gestor : System.Web.UI.Page
    {
        ControllerWeb controllerWeb;

        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = (ControllerWeb)Session["item"];
        }

        public void printCourses() {
            foreach (Course course in controllerWeb.courses)
            {
                Session["CourseId"] = course.getId();
                Response.Write("<br><a href = \"./changeCourseTemplate.aspx?CourseId=" + Session["CourseId"].ToString() +  "\">" + course.getId() + " - " + course.getName() + "</a>");
            }
        }

        public void printPlans()
        {
            foreach (CoursePlan plan in controllerWeb.getPlans())
            {
                Session["planId"] = plan.getId();
                Response.Write("<br><b><a href = \"./courseTemplates.aspx?planId=" + Session["planId"].ToString() + "\">" + plan.getId() + " - " + plan.getName() + "</a></b>");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using proyectoDiseno2.Clases;

namespace proyectoDiseno2.GUI
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        ControllerWeb controllerWeb;

        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = (ControllerWeb)Session["item"];
        }

        protected void button_addUser_Click(object sender, EventArgs e)
        {
            //add user
            controllerWeb.signUp(textbox_name_sign.Text, textbox_username_sign.Text, textbox_password_sign.Text, RadioButtonList1.SelectedItem.ToString(), textbox_id_sign.Text);
         
            Response.Redirect("Admin.aspx");
           
        }
    }
}
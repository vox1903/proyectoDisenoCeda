﻿using proyectoDiseno2.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using proyectoDiseno2.Clases.FileHandling;

namespace proyectoDiseno2.GUI
{
    public partial class courseTemplates : System.Web.UI.Page
    {
        ControllerWeb controllerWeb;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            controllerWeb = (ControllerWeb)Session["item"];
        }

        public void printCourses()
        {
  
            String planId = Request.QueryString["planId"];

            foreach (CoursePlan plan in controllerWeb.getPlans())
            {

                if (plan.getId().Equals(planId)) {

                    foreach (Course course in plan.getCourses())
                    {
                        Response.Write("<br><a href = \"./changeCourseTemplate.aspx?CourseId=" + course.getId() + "&planId=" + planId + "\">" + course.getId() + " - " + course.getName() + "</a>");
                    }
                   
                }

            }
           

        }

    }
}

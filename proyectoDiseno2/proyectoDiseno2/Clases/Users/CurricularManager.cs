﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using proyectoDiseno2.Clases.FileHandling;

namespace proyectoDiseno2.Clases.Users
{
    public class CurricularManager : User
    {
        LinkedList<CoursePlan> coursePlans;

        public CurricularManager(string pUssername, string pPassword, string pName, string pId)
        {
            coursePlans = new LinkedList<CoursePlan>();
            ussername = pUssername;
            password = pPassword;
            name = pName;
            id = pId;
        }

        public void setEditors(LinkedList<string> editors, string nameCoursePlan, string nameCourse)
        {
            foreach (CoursePlan c in coursePlans)
            {
                if (c.getName().Equals(nameCoursePlan))
                {
                    c.setEditors(nameCourse, editors);
                }
            }
        }

        public void uploadPlan(string path)
        {

        }

        public void generateTempaltes(string nameCoursePlan)
        {

        }

        public void saveChanges(string nameCoursePlan)
        {

        }

        public void setCourseTemplateActive(string nameCoursePlan, string nameCourse)
        {
            foreach (CoursePlan c in coursePlans)
            {
                if (c.getName().Equals(nameCoursePlan))
                {
                    c.setCourseActive(nameCourse);
                }
            }
        }


        public void saveChanges(Template template)
        {
            TemplateEditor templateEditor = new TemplateEditor();
            templateEditor.saveChanges(this, template);
        }

        public Template editTemplate(string pName)
        {
            TemplateEditor templateEditor = new TemplateEditor();
            return templateEditor.editFile(this, pName);
        }


        public LinkedList<Component> readComponents(XmlNodeList nodes)
        {
            LinkedList<Component> current = new LinkedList<Component>();

            foreach (XmlNode node in nodes)
            {
                if (node.Name.Equals("part"))
                {
                    current.AddLast(new Part(node.Attributes["name"].Value, node.InnerText));
                }
                else
                {
                    current.AddLast(new Section(node.Attributes["name"].Value, readComponents(node.ChildNodes)));
                }
            }
            return current;
        }


        public LinkedList<string> getNameTemplates()
        {
            LinkedList<string> names = new LinkedList<string>();

            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\templates.xml");
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/templates/template");

            foreach (XmlNode node in nodes)
            {
                names.AddLast(node.Attributes["name"].Value.ToString());

            }
            return names;

        }

        public LinkedList<CoursePlan> getPlans()
        {
            CoursePlanEditor coursePlanEditor = new CoursePlanEditor();
            return coursePlanEditor.getPlans();
        }


    }
}
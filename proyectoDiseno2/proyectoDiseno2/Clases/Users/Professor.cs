﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proyectoDiseno2.Clases.FileHandling;
using System.Xml;

namespace proyectoDiseno2.Clases.Users
{
    public class Professor : User
    {
        LinkedList<Course> editor;      //NAME OF TEMPLATES AVAILABLE TO EDIT

        public Professor(string pUssername, string pPassword, string pName, string pId)
        {
            ussername = pUssername;
            password = pPassword;
            name = pName;
            id = pId;
            editor = new LinkedList<Course>();
        }

        public void saveChanges(Template template)
        {
            TemplateEditor templateEditor = new TemplateEditor();
            templateEditor.saveChanges(this, template);
        }

        public Template editTemplate(string pName)
        {
            TemplateEditor templateEditor = new TemplateEditor();
            return templateEditor.editFile(this, pName);
        }

        public LinkedList<string> getNameTemplates()
        {
            LinkedList<string> names = new LinkedList<string>();

            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\templates.xml");
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/templates/template");

            foreach (XmlNode node in nodes)
            {
                names.AddLast(node.Attributes["name"].Value.ToString());

            }
            return names;

        }

        public void addCourse(Course pCourse) {
            editor.AddLast(pCourse);
        }

        public LinkedList<Course> getCourses()
        {
            return editor;
        }

    }
}
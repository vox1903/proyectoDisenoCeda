﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proyectoDiseno2.Clases.FileHandling;
using System.Xml;


namespace proyectoDiseno2.Clases.Users
{
    public class Admin : User
    {
        public Admin(string pUssername, string pPassword, string pName, string pId)
        {
            ussername = pUssername;
            password = pPassword;
            name = pName;
            id = pId;
        }



        public LinkedList<string> getNameTemplates()
        {
            LinkedList<string> names = new LinkedList<string>();

            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "Oficial\\names.xml");
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/templates/template");

            foreach (XmlNode node in nodes)
            {
                names.AddLast(node.InnerText);
            }
            return names;

        }

        public void saveChanges(Template template)
        {
            TemplateEditor templateEditor = new TemplateEditor();
            templateEditor.saveChanges(this, template);
        }

        public Template editTemplate(string pName)
        {
            TemplateEditor templateEditor = new TemplateEditor();
            return templateEditor.editFile(this, pName);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proyectoDiseno2.Clases.Users;
using System.Xml;
using System.Xml.Linq;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection;
using System.IO;
using proyectoDiseno2.Clases.FileHandling;
using proyectoDiseno2.Output;

namespace proyectoDiseno2.Clases
{
    public class ControllerWeb
    {
        private LinkedList<User> users;
        private User currentUser;
        public LinkedList<Course> courses;
        

        public ControllerWeb()
        {
            users = new LinkedList<User>();
            loadUsers();
                                   
        }

        public bool signIn(string pUssername, string pPassword)
        {
            foreach (User user in users)
            {
                if (user.getUssername().Equals(pUssername) & user.getPassword().Equals(pPassword))
                {
                    currentUser = user;
                    return true;
                }
            }
            return false;
        }

        public void signUp(String pName, String pUsername, String pPassword, String pType, String pId)
        {
            // Load existing clients and add new 
            XElement xml = XElement.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\users.xml");
            xml.Add(new XElement("user",
            new XElement("name", pName),
            new XElement("username", pUsername),
            new XElement("id", pId),
            new XElement("password", pPassword),
            new XElement("type", pType)));
            xml.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\users.xml");

            loadUsers();

        }
        

        public void loadUsers()
        {
            String name;
            String ussername;
            String id;
            String password;
            String type;

            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\users.xml");
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/users/user");

            foreach (XmlNode node in nodes)
            {
                name = node.SelectSingleNode("name").InnerText;
                ussername = node.SelectSingleNode("username").InnerText;
                id = node.SelectSingleNode("id").InnerText;
                password = node.SelectSingleNode("password").InnerText;
                type = node.SelectSingleNode("type").InnerText;
                
                if (type.Equals("professor"))
                {
                    users.AddLast(new Professor(ussername,password,name,id));

                }else if (type.Equals("gestor"))
                {
                    users.AddLast(new CurricularManager(ussername, password, name, id));
                }
                else if (type.Equals("admin"))
                {
                    users.AddLast(new Admin(ussername, password, name, id));
                }

            }
            
        }

        public LinkedList<string> getNameTemplates()
        {
            if (currentUser is Admin)
            {
                return ((Admin)currentUser).getNameTemplates();
            }else if (currentUser is Admin)
            {
                return ((Professor)currentUser).getNameTemplates();
            }
            else if (currentUser is Admin)
            {
                return ((CurricularManager)currentUser).getNameTemplates();
            }
            else
                return null;
        }

        public LinkedList<CoursePlan> getPlans()
        {
            if (currentUser is CurricularManager)
            {
                return ((CurricularManager)currentUser).getPlans();
            }
            return null;
        }


        public User getCurrentUser() {
            return currentUser;
        }

        public void saveChanges(Template template)
        {
            if (currentUser is Admin)
            {
                ((Admin)currentUser).saveChanges(template);
            }
            else if (currentUser is Admin)
            {
                ((Professor)currentUser).saveChanges(template);
            }
            else if (currentUser is Admin)
            {
                ((CurricularManager)currentUser).saveChanges(template);
            }
        }




        public Template editTemplate(string pName)
        {
            if (currentUser is Admin)
            {
                return ((Admin)currentUser).editTemplate(pName);
            }
            else if (currentUser is Professor)
            {
                return ((Professor)currentUser).editTemplate(pName);
            }
            else if (currentUser is CurricularManager)
            {
                return ((CurricularManager)currentUser).editTemplate(pName);
            }
            return null;
        }


        public LinkedList<Component> readComponents(XmlNodeList nodes)
        {
            LinkedList<Component> current = new LinkedList<Component>();

            foreach (XmlNode node in nodes)
            {
                if (node.Name.Equals("part"))
                {
                    current.AddLast(new Part(node.Attributes["name"].Value, node.InnerText));
                }
                else
                {
                    current.AddLast(new Section(node.Attributes["name"].Value, readComponents(node.ChildNodes)));
                }
            }
            return current;
        }

        public LinkedList<User> getUsers() {
            return users;
        }


    }
}
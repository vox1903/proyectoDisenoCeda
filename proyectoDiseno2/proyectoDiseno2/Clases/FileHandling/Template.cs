﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proyectoDiseno2.Clases.Users;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class Template 
    {
        public enum State {
            DRAFT,
            NEW,
            SAVED,
            DONE
        };

        private string location;
        private string period;
        private string name;
        private DateTime creationDate;
        private DateTime lastUpdate;
        private State state;
        LinkedList<Component> components;
        LinkedList<String> editors;

        public Template(string pName, string pLocation)
        {
            name = pName;
            location = pLocation;
            state = State.NEW;
            components = new LinkedList<Component>();
            editors = new LinkedList<String>();
        }

        public Template(string pName)
        {
            name = pName;
            state = State.NEW;
            components = new LinkedList<Component>();
            editors = new LinkedList<String>();
        }

        public void setComponents(LinkedList<Component> pComponents)
        {
            this.components = pComponents;
        }

        public LinkedList<Component> getComponents()
        {
            return this.components;
        }

        public void addEditor(string professor)
        {
            editors.AddLast(professor);
        }

        public void setState(State pState)
        {
            this.state = pState;
        }
        public void setName(string pName)
        {
            this.name = pName;
        }
        public void setLocation(string pLocation)
        {
            this.location = pLocation;
        }

        public LinkedList<string> getEditors()
        {
            return this.editors;
        }

        public string getState()
        {
            if (state == State.DONE)
            {
                return "DONE";
            }else if (state == State.DRAFT)
            {
                return "DRAFT";
            }
            else if (state == State.NEW)
            {
                return "NEW";
            }
            else if (state == State.SAVED)
            {
                return "SAVED";
            }
            return null;
        }

        public string getName()
        {
            return this.name;
        }

        public string getLocation()
        {
            return this.location;
        }
        public string getPeriod()
        {
            return this.period;
        }

        public object Clone()
        {
            using (var memStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter(
                   null,
                   new StreamingContext(StreamingContextStates.Clone));
                binaryFormatter.Serialize(memStream, this);
                memStream.Seek(0, SeekOrigin.Begin);
                return binaryFormatter.Deserialize(memStream);
            }
        }


    }
}
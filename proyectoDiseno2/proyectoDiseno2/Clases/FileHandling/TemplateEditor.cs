﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using proyectoDiseno2.Clases.Users;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class TemplateEditor
    {


        public Template editFile(User pUser,string pName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\Oficial\\"+ pName + ".xml");
            XmlNodeList templates = doc.DocumentElement.SelectNodes("/template");
            foreach (XmlNode template in templates)
            {
                if (template.Attributes["name"].Value.Equals(pName))
                {
                    Template loadedTemplate = new Template(template.Attributes["name"].Value,
                        template.Attributes["location"].Value);
                    foreach (XmlNode node in template.ChildNodes)
                    {
                        if (node.Name.Equals("state"))
                        {
                            loadedTemplate.setState((Template.State)Enum.Parse(typeof(Template.State), node.InnerText, true));
                        }
                        else if (node.Name.Equals("editors"))
                        {
                            foreach (XmlNode editorNode in node.ChildNodes)
                            {
                                loadedTemplate.addEditor(editorNode.InnerText);
                            }
                        }
                        else if (node.Name.Equals("components"))
                        {
                            loadedTemplate.setComponents(readComponents(node.ChildNodes));
                        }
                    }
                    return loadedTemplate;
                }
            }
            return null;
        }


        public LinkedList<Component> readComponents(XmlNodeList nodes)
        {
            LinkedList<Component> current = new LinkedList<Component>();

            foreach (XmlNode node in nodes)
            {
                if (node.Name.Equals("part"))
                {
                    current.AddLast(new Part(node.Attributes["name"].Value, node.InnerText));
                }
                else
                {
                    current.AddLast(new Section(node.Attributes["name"].Value, readComponents(node.ChildNodes)));
                }
            }
            return current;
        }




        public void saveChanges(User pUser, Template template)
        {

            XDocument doc = new XDocument();
            XElement xmlTemplate = new XElement("template",
                new XAttribute("name",template.getName()),
                new XAttribute("location", template.getLocation())
                );

            XElement state = new XElement("state", template.getState());
            XElement editors = new XElement("editors");
            XElement components = new XElement("components");


            foreach (string editor in template.getEditors())
            {
                editors.Add(new XElement("name", editor));
            }

            saveComponents(template.getComponents(), components);

            xmlTemplate.Add(components);
            xmlTemplate.Add(state);
            xmlTemplate.Add(editors);
            doc.Add(xmlTemplate);
            doc.Save(template.getLocation() + "\\" + template.getName() + ".xml");


        }

        public void saveComponents(LinkedList<Component> components, XElement current)
        {
            XElement part;
            XElement section;
            foreach (Component component in components)
            {
                if (component is Part)
                {
                    part = new XElement("part", component.getContent(), new XAttribute("name", component.getName()));
                    current.Add(part);
                }
                else if (component is Section)
                {
                    section = new XElement("section", new XAttribute("name", component.getName()));
                    saveComponents(((Section)component).getComponents(), section);
                    current.Add(section);
                }
            }
        }



    }
}
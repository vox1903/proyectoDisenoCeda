﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class CoursePlan
    {
        private string name;
        private string id;
        private Template.State state;
        private LinkedList<Course> courses;
        

        public CoursePlan(string pName,string pId, Template.State pState)
        {
            this.name = pName;
            this.id = pId;
            this.state = pState;
            courses = new LinkedList<Course>();
            
        }

        public void addCourse(string pName, string pId, Template.State pState)
        {
            courses.AddLast(new Course(pName, pId));
        }

        public void setEditors(string pName, LinkedList<string> pEditors)
        {
            foreach (Course c in courses)
            {
                if (c.getName().Equals(pName))
                {
                    c.setEditors(pEditors);
                }
            }
        }
        
        public void setCourseActive(string pName)
        {
            foreach (Course c in courses){
                if (c.getName().Equals(pName))
                {
                    c.setGenerateTemplate(true);
                }
            }
        }

        public string getName()
        {
            return name;
        }

        public string getId()
        {
            return id;
        }

        public LinkedList<Course> getCourses() {
            return courses;
        }


    }
}
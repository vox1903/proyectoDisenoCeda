﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class CoursePlanEditor
    {
        public LinkedList<CoursePlan> plans;

        public CoursePlanEditor()
        {
            plans = new LinkedList<CoursePlan>();
            loadPlans();
        }

        public LinkedList<CoursePlan> getPlans()
        {
            return plans;
        }


        public void loadPlans()
        {

            String id;
            String name;

            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\\courses.xml");
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/plans/plan");

            foreach (XmlNode node in nodes)
            {

                id = node.SelectSingleNode("idPlan").InnerText;
                name = node.SelectSingleNode("namePlan").InnerText;

                CoursePlan plan = new CoursePlan(name, id, Template.State.SAVED);

                plans.AddLast(plan);

                XmlNodeList nodesc = doc.DocumentElement.SelectNodes("/plans/plan/courses/course");

                foreach (XmlNode nodec in nodesc)
                {

                    id = nodec.SelectSingleNode("id").InnerText;
                    name = nodec.SelectSingleNode("name").InnerText;

                    plan.addCourse(name, id, Template.State.SAVED);

                }

            }

        }



    }
}
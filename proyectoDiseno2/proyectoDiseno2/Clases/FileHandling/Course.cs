﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class Course
    {
        private string name;
        private string id;
        private bool generateTemplate;
        private LinkedList<String> editors;

        public Course(string pName, string pId)
        {
            this.name = pName;
            this.id = pId;
            generateTemplate = false;
            editors = new LinkedList<string>();
        }

        public Course(string pName, string pId, bool pGenerateTemplate)
        {
            this.name = pName;
            this.id = pId;
            generateTemplate = pGenerateTemplate;
            editors = new LinkedList<string>();
        }

        public void setEditors(LinkedList<string> pEditors)
        {
            editors = pEditors;
        }

        public void setGenerateTemplate(bool pGenerateTemplate)
        {
            this.generateTemplate = pGenerateTemplate;
        }

        public bool getGenerateTemplate()
        {
            return generateTemplate;
        }

        public string getName()
        {
            return name;
        }

        public string getId()
        {
            return id;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class Section : Component
    {
        LinkedList<Component> components;

        public Section(string pName, int pIndex)
        {
            this.name = pName;
            this.index = pIndex;
            components = new LinkedList<Component>();
        }

        public Section(string pName, LinkedList<Component> pComponents)
        {
            this.name = pName;
            components = new LinkedList<Component>();
            components = pComponents;
        }

        public LinkedList<Component> getComponents()
        {
            return this.components;
        }


    }
}
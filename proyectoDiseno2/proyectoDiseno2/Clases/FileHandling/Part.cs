﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyectoDiseno2.Clases.FileHandling
{
    public class Part : Component
    {

        public Part(string pName, string pContent)
        {
            this.name = pName;
            this.content = pContent;

        }

        public Part(string pName)
        {
            this.name = pName;

        }
        
        public void setContent(string pContent)
        {
            content = pContent;
        }


    }
}
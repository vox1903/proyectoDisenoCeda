﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyectoDiseno2.Clases.FileHandling
{
    public abstract class Component
    {
        protected String name;
        protected int index;
        protected string content;

        public string getName()
        {
            return name;
        }

        public int getIndex()
        {
            return index;
        }

        public void setName(string pName)
        {
            name = pName;
        }

        public void setIndex(int pIndex)
        {
            index = pIndex;
        }

        public String getContent()
        {
            return content;
        }

        public void setContent(string pContent)
        {
            content = pContent;
        }

    }
}